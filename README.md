# Sprawozdanie z fizyki (ćwiczenie E3)

### Treść ćwiczenia
[Ćwiczenie E3](https://ftims.pg.edu.pl/documents/10673/46008532/cwiczenieE3.pdf)

### Kompilacja
`pdflatex fizyka.tex`

### Rezultat
[Plik fizyka.pdf](https://gitlab.com/marcel2012/fizyka-sprawozdanie-nr-2/-/jobs/artifacts/master/raw/fizyka.pdf?job=build)
